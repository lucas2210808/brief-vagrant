# brief-vagrant


Vagrant init : Initialise le repertoire en environnement Vagrant et crée un fichier "Vagrantfile" s'il n'y en a pas déjà un. On peut, à la fin de la commande, ajouter un argument pour pré-remplir la box dans le fichier "Vagrantfile"
Cette box est stockée dans un répertoir ~/.vagrant.d/boxes

Vagrant validate : Cette commande valide le fichier "Vagrantfile"

Vagrant up : Cette commande crée et configure une machine en fonction de ce qu'on a mit dans le fichier "Vagrantfile"

Vagrant status : Donne l'état des machines Vagrant que l'on gère

Vagrant global-status : Cette commande donne l'état de tous les environnements Vagrant actifs sur le système pour l'utilisateur actuellement connecté

Vagrant ssh : Va nous connecter en ssh à une machine Vagrant en marche et nous donner accès au shell

Vagrant halt : Cette commande éteint la machine à laquelle on est actuellement connecté

Vagrant destroy : Cette commande arrete la machine Vagrant en marche et détruit les ressources qui ont étaient crées pendant le processus de création de la machine

Vagrant box add ubuntu/trusty64 : Ajoute une box à Vagrant à l'adresse "ubuntu/trusty64"

Vagrant init ubuntu/trusty64 -h : On Initialise le repertoire en environnement Vagrant dans la box "ubuntu/trusty64" précedemment ajoutée. L'option "-h" nous de l'aide sur la manière d'utiliser la commande "vagrant init" et les options disponibles
